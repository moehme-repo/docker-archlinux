FROM archlinux/base:latest
MAINTAINER Marty Oehme <marty.oehme@gmail.com>

RUN echo -e "\n[multilib]\nInclude = /etc/pacman.d/mirrorlist\n\n[moehme-repo]\nSigLevel = Never\nServer = https://moehme-repo.gitlab.io/repository" >> /etc/pacman.conf \
	&& pacman -Suyy --needed --noconfirm --noprogressbar base base-devel git \
	&& useradd -m -U build \
	&& echo "build ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/build
